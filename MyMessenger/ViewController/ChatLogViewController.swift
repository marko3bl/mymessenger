//
//  ChatLogViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit
import MobileCoreServices
import AVKit

protocol UpdateUIDelegate: class {
    func updateUI(value: Int64, totalValue: Int64)
    func completedUpdate(_ status: Bool)
}

class ChatLogViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageTextField: UITextField!
    @IBOutlet weak var conteinerViewBottomAnchor: NSLayoutConstraint!
    
    //MARK: - Properties
    private var sendMessageFirebaseContoller: SendMessageFirebaseController? {
        didSet {
            sendMessageFirebaseContoller?.updateUIDelegate = self
        }
    }
    
    private var fetchMessagesFirebaseController: FetchMessagesFirebaseController?
    private var photoMessage = PhotoMessage()
    private var progressView = UIProgressView()
    private let userDefaults = UserDefaults.standard
    
    var selectedUser: User? {
        didSet {
            fetchMessagesFirebaseController = FetchMessagesFirebaseController()
            loadMessages()
        }
    }
    
    var messages = [Message]()
    
    private let backgroundImageView: UIImageView = {
        let iv = UIImageView()
        iv.contentMode = .scaleAspectFill
        return iv
    }()
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.tableFooterView = UIView()
        messageTextField.delegate = self

        tableView.dataSource = self
        tableView.delegate = self
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 1000

        if let imageName = userDefaults.value(forKey: "backgroundImageKey") as? String {
            backgroundImageView.image = UIImage(named: imageName)
            tableView.backgroundView = backgroundImageView
        } else {
            tableView.backgroundView = nil
        }
        
        
        setupKeyboardObservers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        sendMessageFirebaseContoller = SendMessageFirebaseController()
        navigationItem.title = selectedUser?.name
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        sendMessageFirebaseContoller = nil
        NotificationCenter.default.removeObserver(self)
        fetchMessagesFirebaseController?.removeAllObservers()
    }

    //MARK: - IBAcions
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        sendMessage()
    }
    
    @IBAction func backButtonTapped(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func uploadImageButtonTapped(_ sender: UIButton) {
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.mediaTypes = [kUTTypeImage as String, kUTTypeMovie as String]
        
        present(imagePicker, animated: true, completion: nil)
    }
    
    
    //MARK: - Helper methods
    private func sendMessage() {
        guard let message = messageTextField.text else {return}
        guard message != "", selectedUser?.id != nil else {return}
        sendMessageFirebaseContoller?.sendMessage(text: message, toID: selectedUser!.id!)
        messageTextField.text = ""
    }
    
    private func loadMessages() {
        guard let id = selectedUser?.id else {return}
        fetchMessagesFirebaseController?.fetchMessagesFromUser(id: id, completion: { (messages) in
            self.messages = messages
            DispatchQueue.main.async {
                self.tableView.reloadData()
                self.tableView.scrollToRow(at: IndexPath(row: self.messages.count-1, section: 0), at: .bottom, animated: true)
            }

        })
    }
    
    //MARK: - Handle Keyboard
    private func setupKeyboardObservers() {
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIWindow.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboard), name: UIWindow.keyboardWillHideNotification, object: nil)
    }
    
    @objc private func handleKeyboard(notification: NSNotification) {
        guard let keyboardFrame = (notification.userInfo?[UIWindow.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue else {return}
        guard let keyboardDuration = (notification.userInfo?[UIWindow.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue else {return}
        
        let bottomSafeArea = view.safeAreaInsets.bottom
        let isKeyboardShowing = notification.name == UIWindow.keyboardWillShowNotification
        
        conteinerViewBottomAnchor.constant = isKeyboardShowing ? -keyboardFrame.height + bottomSafeArea : 0

        UIView.animate(withDuration: keyboardDuration, animations: {
            self.view.layoutIfNeeded()
        }) { (completed) in
        
            if isKeyboardShowing {
                guard self.messages.count > 0 else {return}
                self.tableView.scrollToRow(at: IndexPath(row: self.messages.count-1, section: 0), at: .bottom, animated: true)
            }
        }
    }
    
    //MARK: - Change phone orientation
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {

        guard let frame = UIApplication.shared.keyWindow?.frame else {return}
        photoMessage.setupImageHeightInLandscapeMode(frame: frame)
        
    }
    
    //MARK: - Object lifecycles
    deinit {
        print("ChatLogViewController was deleted!")
    }
}

//MARK: - TableView Datasource
extension ChatLogViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = messages[indexPath.row]

        if message.videoUrl != nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "videoCell", for: indexPath) as! VideoCell
            cell.delegate = photoMessage
            cell.videoPlayerDelegate = self
            setup(cell: cell, message: message)
            cell.message = message
            cell.currentUserId = fetchMessagesFirebaseController?.currentUserID
            return cell
        } else if message.imageUrl == nil {
            let cell = tableView.dequeueReusableCell(withIdentifier: "chatMessageCell", for: indexPath) as! MessageCell
            setup(cell: cell, message: message)
            cell.message = message
            cell.currentUserId = fetchMessagesFirebaseController?.currentUserID
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "imageMessageCell", for: indexPath) as! ImageMessageCell
            cell.delegate = photoMessage
            setup(cell: cell, message: message)
            cell.message = message
            cell.currentUserId = fetchMessagesFirebaseController?.currentUserID
            return cell
        }
    }
    
    private func setup(cell: BasicCell, message: Message) {
        if message.fromID == selectedUser?.id {
            cell.profileImageView.loadImageUsingCacheWith(urlString: selectedUser!.profileImageURL)
            cell.profileImageView.isHidden = false
        } else {
            cell.profileImageView.isHidden = true
        }
    }
}

//MARK: - UITableView Delegate
extension ChatLogViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let message = messages[indexPath.row]
        guard let imageHeight = message.imageHeight?.floatValue, let imageWidth = message.imageWidth?.floatValue else { return calculateTextHeight(text: message.text) }
        
        let height = CGFloat(imageHeight / imageWidth * 250)
        
        return height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        messageTextField.endEditing(true)
    }
    
    private func calculateTextHeight(text: String) -> CGFloat {
        let size = CGSize(width: 250, height: 1000)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let rect = NSString(string: text).boundingRect(with: size, options: options, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 16)], context: nil)
        return rect.height + 32
    }
}

//MARK: - TextField Delegate
extension ChatLogViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == messageTextField {
            textField.resignFirstResponder()
            sendMessage()
            return false
        }
        
        return true
    }
}

//MARK: - UIImagePickerController Delegate
extension ChatLogViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        //we selected a video
        if let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? URL {
            
            uploadVideoToFirebaseStorage(videoUrl)
            
        } else {
            //we selected an image
            var selectedImage: UIImage?
            
            if let editingImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                
                selectedImage = editingImage
                
            } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                
                selectedImage = originalImage
            }
            
            if let image = selectedImage {
                uploadImageToFirebaseStorage(image)
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
    
    private func uploadImageToFirebaseStorage(_ image: UIImage) {
        sendMessageFirebaseContoller = SendMessageFirebaseController()
        sendMessageFirebaseContoller?.sendMessageWithImage(image, toId: selectedUser!.id!)
    }
    
    private func uploadVideoToFirebaseStorage(_ videoUrl: URL) {
        sendMessageFirebaseContoller = SendMessageFirebaseController()
        sendMessageFirebaseContoller?.sendVideoMessage(videoUrl, toId: selectedUser!.id!)
        navigationItem.setupTitleViewWithProgressBar(view: progressView, alertText: "Sending video...")
    }
}

//MARK: - Implement Video Player Delegate
extension ChatLogViewController: VideoPlayerDelegate {
    
    func playVideo(url: URL) {
        
        let player = AVPlayer(url: url)
        let playerVC = AVPlayerViewController()
        playerVC.player = player
        
        present(playerVC, animated: true) {
            player.play()
        }
    }
}

//MARK: - Implement UpadteUIDelegate
extension ChatLogViewController: UpdateUIDelegate {
    func updateUI(value: Int64, totalValue: Int64) {
        guard value > 0 else {return}
        let progress = Float(value) / Float(totalValue)
        progressView.progress = progress
    }
    
    func completedUpdate(_ status: Bool) {
        if status {
            navigationItem.title = selectedUser?.name
            navigationItem.titleView = nil
        }
    }
}
