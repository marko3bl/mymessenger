//
//  FetchDataFirebaseController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation
import Firebase

class FetchDataFirebaseController {
    
    private var ref: DatabaseReference
    
    var currentUserUUID: String? {
        return Auth.auth().currentUser?.uid
    }
    
    init() {
        ref = Database.database().reference().child("users")
    }
    
    func fetchUser(completion: @escaping (User?)->Void) {
        
        guard let uuid = currentUserUUID else {
            print("No users")
            completion(nil)
            return
        }
        
        ref.child(uuid).observeSingleEvent(of: .value) { (snapshot) in
            let value = snapshot.value as? [String: Any] ?? [:]
            let user = User(dictionary: value)
            completion(user)
        }
    }
    
    func fetchUserWith(uuid: String, completion: @escaping (User) -> Void) {
        
        ref.child(uuid).observeSingleEvent(of: .value) { (snapshot) in
            
            let value = snapshot.value as? [String: Any] ?? [:]
            var user = User(dictionary: value)
            user.id = snapshot.key
            completion(user)
            
        }
        
    }
    
    func fetchContacts(completion: @escaping ([User]) -> Void) {
        
        var users = [User]()
        
        guard let uuid = currentUserUUID else {
            print("No contacts")
            completion([])
            return
        }
        
        ref.observe(.value) { (snapshot) in
            
            let values = snapshot.value as? [String:Any]
            let contacts = values?.filter({$0.key != uuid})
            contacts?.forEach({
                let dictionary = $0.value as? [String: Any] ?? [:]
                var user = User(dictionary: dictionary)
                user.id = $0.key
                users.append(user)
            })
            
            users.sort(by: {$0.name < $1.name})
            completion(users)
        }
    }
    
    func removeAllObservers() {
        ref.removeAllObservers()
    }
}
