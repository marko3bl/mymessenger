//
//  FetchMessagesFirebaseController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation
import Firebase

class FetchMessagesFirebaseController {
    
    private var messagesRef: DatabaseReference!
    private var userMessagesRef: DatabaseReference!
    
    private let dispatchGroup = DispatchGroup()
    
    var currentUserID: String? {
        return Auth.auth().currentUser?.uid
    }
    
    init() {
        messagesRef = Database.database().reference().child("messages")
        userMessagesRef = Database.database().reference().child("user-messages")
    }
    
    func fetchMessages(completion: @escaping ([Message]) -> Void) {
        
        guard let uuid = currentUserID else {return}
        
        var messages = [Message]()
        var messageDictionary = [String: Message]()
        
        userMessagesRef.child(uuid).observe(.childAdded) { (snapshot) in
            
            let userId = snapshot.key
            
            self.userMessagesRef.child(uuid).child(userId).observe(.childAdded, with: { (snapshot) in
      
                let messageId = snapshot.key
                
                self.dispatchGroup.enter()
                
                self.messagesRef.child(messageId).observeSingleEvent(of: .value, with: { (snapshot) in
      
                    let dictionary = snapshot.value as? [String: Any] ?? [:]
                    let message = Message(dictionary: dictionary)
                    
                    guard let chatPartnerId = message.chatPartnerID(userId: uuid) else {return}

                    messageDictionary[chatPartnerId] = message
                    messages = Array(messageDictionary.values)
                    
                    self.dispatchGroup.leave()

                })
                
                self.dispatchGroup.notify(queue: .main, execute: {
                    messages.sort(by: {$0.timestamp > $1.timestamp})
                    completion(messages)
                })
            })
        }
    }
    
    func fetchMessagesFromUser(id: String, completion: @escaping ([Message]) -> Void) {
        guard let uuid = currentUserID else {return}
        var userMessages = [Message]()
        
        userMessagesRef.child(uuid).child(id).observe(.childAdded) { (snapshot) in
            
            self.messagesRef.child(snapshot.key).observeSingleEvent(of: .value, with: { (snapshot) in
                let dictionary = snapshot.value as? [String: Any] ?? [:]
                let message = Message(dictionary: dictionary)
                
                userMessages.append(message)
                completion(userMessages)
            })
        }
    }
    
    func removeAllObservers() {
        messagesRef.removeAllObservers()
        userMessagesRef.removeAllObservers()
    }
}
