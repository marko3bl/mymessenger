//
//  ChooseBackgroundViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/17/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class ChooseBackgroundViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var backgroundImageView: UIImageView! {
        didSet {
            backgroundImageView.image = UIImage(named: imageName)
        }
    }
    
    //MARK: - Properties
    private let userDefaults = UserDefaults.standard
    private let backgroundImageKey = "backgroundImageKey"
    
    var imageName: String!

    //MARK: - IBActions
    @IBAction func chooseButtonTapped(_ sender: UIBarButtonItem) {
        userDefaults.set(imageName, forKey: backgroundImageKey)
        performSegue(withIdentifier: "unwindSegueToMoreVC", sender: self)
    }
}
