//
//  ViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/12/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class ChatsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    private var fetchDataFirebaseController: FetchDataFirebaseController?
    private var fetchMessagesFirebaseController: FetchMessagesFirebaseController?
    
    var messages: [Message]? {
        didSet {
            tableView.reloadData()
        }
    }
    
    var chatPartner: User? {
        didSet {
            performSegue(withIdentifier: "chatLogSegue", sender: self)
        }
    }
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fetchDataFirebaseController = FetchDataFirebaseController()
        fetchMessagesFirebaseController = FetchMessagesFirebaseController()
        
        fetchCurrentUser()
        loadMessages()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        fetchMessagesFirebaseController?.removeAllObservers()
    }

    //MARK: - Helper methods
    private func fetchCurrentUser() {
        fetchDataFirebaseController?.fetchUser(completion: { (user) in
            guard let currentUser = user else {return}
            DispatchQueue.main.async {
                self.navigationItem.setupTitleViewWith(name: currentUser.name, profileImageURL: currentUser.profileImageURL)
            }
        })
    }
    
    private func loadMessages() {
        
        fetchMessagesFirebaseController?.fetchMessages(completion: { (messages) in
            self.messages = messages
        })
    }
    
    
    //MARK: - Object lifecycle
    deinit {
        print("ChatsViewController was deleted!")
    }

}

//MARK: - TableView Datasource
extension ChatsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! ChatCell
        
        guard let message = messages?[indexPath.row] else {return UITableViewCell()}
        cell.message = message
        
        guard let id = message.chatPartnerID(userId: fetchDataFirebaseController?.currentUserUUID) else {
            return UITableViewCell()
        }
        
        fetchDataFirebaseController?.fetchUserWith(uuid: id, completion: { (user) in
            cell.user = user
        })
        
        return cell
    }    
}

//MARK: - TableView Delegate
extension ChatsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let message = messages?[indexPath.row] else {return}
        guard let chatPartnerID = message.chatPartnerID(userId: fetchDataFirebaseController?.currentUserUUID) else {return}
    
        fetchDataFirebaseController?.fetchUserWith(uuid: chatPartnerID, completion: { (user) in
            
            DispatchQueue.main.async {
                self.chatPartner = user
            }
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}

//MARK: - Prepare for segue
extension ChatsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chatLogSegue" {
            guard let navController = segue.destination as? UINavigationController,
                let chatLogVC = navController.viewControllers.first as? ChatLogViewController else {
                return
            }
            
            chatLogVC.selectedUser = chatPartner
        }
    }
}
