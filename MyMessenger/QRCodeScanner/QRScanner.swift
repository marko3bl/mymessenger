//
//  QRScanner.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/28/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation

class QRScanner: NSObject {
    
    //MARK: - Private properties
    // AVCaptureSesstion is used to perform real-time capture
    // and to coordinate the flow the data from the video input device to output
    private var captureSession: AVCaptureSession?
    private var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    //MARK: - QRCodeProvider delegate
    weak var delegate: QRCodeProvider?
    
    //MARK: - Object Lifecycle
    init(delegate: QRCodeProvider) {
        super.init()
        
        self.delegate = delegate
        
        captureSession = AVCaptureSession()
        
        //Define capture device
        guard let captureDevice = AVCaptureDevice.default(for: AVMediaType.video) else {
            print("Failed to get the camera device")
            return
        }
        
        do {
            
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            captureSession?.addInput(input)
            
            // Set up output
            let captureMetadataOutput = AVCaptureMetadataOutput()
            captureSession?.addOutput(captureMetadataOutput)
            
            // Set delegate and use default dispatch queue to execute the call back
            captureMetadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            captureMetadataOutput.metadataObjectTypes = [AVMetadataObject.ObjectType.qr]
            
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    //MARK: - Public interface
    func initializeVideoPreviewLayer(view: UIView) {
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession!)
        videoPreviewLayer?.videoGravity = .resizeAspectFill
        videoPreviewLayer?.frame = view.layer.bounds
        view.layer.addSublayer(videoPreviewLayer!)
    }
    
    func startVideoCapture() {
        captureSession?.startRunning()
    }
    
    func stopVideoCapture() {
        captureSession?.stopRunning()
        delegate?.resetQRFrameView()
    }
    
}

//MARK: - Implement AVCaptureMetadataOutputObjectsDelegate
extension QRScanner: AVCaptureMetadataOutputObjectsDelegate {
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        
        if metadataObjects.count == 0 {
            delegate?.resetQRFrameView()
            delegate?.found(code: "No QR code is detected.")
            return
        }
        
        guard let readableObject = metadataObjects.first as? AVMetadataMachineReadableCodeObject else {return}
        guard let stringValue = readableObject.stringValue else {return}
        guard let qrCodeObject = videoPreviewLayer?.transformedMetadataObject(for: readableObject) else {return}
        
        stopVideoCapture()
        
        AudioServicesPlayAlertSound(kSystemSoundID_Vibrate)
        
        delegate?.setupQrCodeFrameView(qrCodeObject.bounds)
        delegate?.found(code: stringValue)
        
    }
}
