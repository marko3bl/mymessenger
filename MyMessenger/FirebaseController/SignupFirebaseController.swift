//
//  SignupFirebaseController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation
import Firebase

class SignupFirebaseController {
    
    var databaseRef: DatabaseReference!
    
    init() {
        databaseRef = Database.database().reference().child("users")
    }
    
    func createAccount(username: String, email: String, password: String, profileImage: UIImage, completion: @escaping (SignupError?) -> Void) {
        
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            if error != nil {
                completion(.createUserError(error!.localizedDescription))
                return
            }
            
            guard let currentUser = user else {return}
            
            var values = ["username": username, "email": email]
            
            let storage = Storage.storage().reference().child("profile_images").child("\(currentUser.user.uid).jpg")
            
            guard let imageData = profileImage.jpegData(compressionQuality: 0.1) else {return}
            
            storage.putData(imageData, metadata: nil, completion: { [weak self](metadata, error) in
                
                if error != nil {
                    completion(.uploadProfileImageError(error!.localizedDescription))
                }
                
   //             let profileImageURL = metadata?.downloadURL()?.absoluteString ?? ""
                
                storage.downloadURL { (url, error) in
                    if error != nil {
                        print("Faild to find download url: \(error!)")
                    } else {
                        values["profileImageURL"] = url?.absoluteString
                    }
                }
                
                
                self?.databaseRef.child(currentUser.user.uid).setValue(values, withCompletionBlock: { (error, ref) in
                    
                    if error != nil {
                        completion(.userDataNotSet(error!.localizedDescription))
                        return
                    }
                    
                })
                
                completion(nil)
                
            })
        }
    }
    
    
    deinit {
        print("SignupFirebaseController was deleted!")
    }
    
}
