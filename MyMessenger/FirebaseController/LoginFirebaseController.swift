//
//  LoginFirebaseController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/12/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation
import Firebase

class LoginFirebaseController {
    
    fileprivate let databaseRef: DatabaseReference
    
    var currentUserUUID: String? {
        return Auth.auth().currentUser?.uid
    }
    
    init() {
        databaseRef = Database.database().reference()
    }
    
    func login(with email: String, password: String, completion: @escaping (_ success: Bool, _ error: LoginError?) -> Void) {

        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if error != nil {
                completion(false, .loginUserError(error!.localizedDescription))
            }
            
            completion(true, nil)
        }
    }
    
    deinit {
        print("LoginFirebaseController was deleted!")
    }
}
