//
//  MessageCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/29/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class MessageCell: BasicCell {

    private var messageLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        label.numberOfLines = 0
        return label
    }()

    var message: Message! {
        didSet {
            messageLabel.text = message.text
        }
    }
    
    var currentUserId: String! {
        didSet {
            bubbleView.backgroundColor = currentUserId == message.fromID ? UIColor.navyColor : UIColor.softLightBlueColor
            messageLabel.textColor = currentUserId == message.fromID ? UIColor.softLightBlueColor : UIColor.darkNavyColor
            super.alignmentBubbleView(userId: currentUserId, fromId: message.fromID)
        }
    }
    
    internal override func setupViews() {
        super.setupViews()
        
        addSubview(messageLabel)
        
        messageLabel.topAnchor.constraint(equalTo: topAnchor, constant: 12).isActive = true
        messageLabel.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -12).isActive = true
        leadingConstraint = messageLabel.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 16)
        trailingConstraint = messageLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -16)
        messageLabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        
        bubbleView.topAnchor.constraint(equalTo: messageLabel.topAnchor, constant: -8).isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: messageLabel.bottomAnchor, constant: 8).isActive = true
        bubbleView.trailingAnchor.constraint(equalTo: messageLabel.trailingAnchor, constant: 8).isActive = true
        bubbleView.leadingAnchor.constraint(equalTo: messageLabel.leadingAnchor, constant: -8).isActive = true
    }
}
