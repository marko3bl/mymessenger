//
//  InitialViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/15/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class InitialViewController: UIViewController {
    
    //MARK: - UI Properties
    private let logoImageView: UIImageView = {
        let iv = UIImageView()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        iv.image  = UIImage(named: "ChatAppIcon")
        return iv
    }()
    
    //MARK: - Properties
    private let userDefaults = UserDefaults.standard
    private var loginFirebaseController: LoginFirebaseController?
    private var logoutFirebaseController: LogoutFirebaseController?
    
    //MARK: - Key for User Defaults
    private let appFirstTimeOpenedKey = "appFirstTimeOpened"
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        loginFirebaseController = LoginFirebaseController()
        logoutFirebaseController = LogoutFirebaseController()
        
        checkIsUserLoggedIn()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        loginFirebaseController = nil
        logoutFirebaseController = nil
    }
    
    //MARK: - Log out from database
    private func logout() {
        logoutFirebaseController?.signOut(completion: { [weak self] (error) in
            
            if error != nil {
                self?.showAlert(with: error!)
                return
            }
            
            self?.presentLoginVC()
        })
    }
    
    //MARK: - Present specific view controller
    private func presentLoginVC() {
        let vc = instantiateVC(withIdentifier: "loginVC", storyboardName: "Login")
        let topVC = topPresentedVC()
        
        topVC?.present(vc, animated: true, completion: nil)
    }
    
    private func presentChatsVC() {
        let vc = instantiateVC(withIdentifier: "tabBarcontroller", storyboardName: "Main")
        let topVC = topPresentedVC()
        topVC?.present(vc, animated: true, completion: nil)
    }
    
    //MARK: Helper methods
    private func setupView() {
        view.addSubview(logoImageView)
        logoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        logoImageView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        logoImageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.75).isActive = true
        logoImageView.heightAnchor.constraint(equalTo: logoImageView.widthAnchor, multiplier: 9/16).isActive = true
    }
    
    private func checkIsUserLoggedIn() {
        
        if userDefaults.value(forKey: "appFirstTimeOpend") == nil {
            
            userDefaults.set(true, forKey: "appFirstTimeOpend")
            logout()
            
        } else {
            if loginFirebaseController?.currentUserUUID != nil {
                
                presentChatsVC()
                return
            } else {
                presentLoginVC()
            }
        }
    }
    
    private func topPresentedVC() -> UIViewController? {
        var topVC: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
        while topVC?.presentedViewController != nil {
            topVC = topVC!.presentedViewController!
        }
        return topVC
    }
    
    private func storyborad(name: String) -> UIStoryboard {
        return UIStoryboard(name: name, bundle: nil)
    }
    
    private func instantiateVC(withIdentifier: String, storyboardName: String) -> UIViewController {
        let storyboard = storyborad(name: storyboardName)
        let vc = storyboard.instantiateViewController(withIdentifier: withIdentifier)
        return vc
    }
    
    //MARK: - Object lifecycle
    deinit {
        print("InitialVC was deleted!")
    }
}
