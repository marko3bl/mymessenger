//
//  ContactsViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/12/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class ContactsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK: - Properties
    private var fetchDataFirebaseController: FetchDataFirebaseController?
    
    private var users: [User]?
    
    private var selectedUser: User? {
        didSet {
            performSegue(withIdentifier: "chatLogSegue", sender: self)
        }
    }
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        fetchDataFirebaseController = FetchDataFirebaseController()
        
        fetchDataFirebaseController?.fetchContacts(completion: { [weak self](users) in
            
            DispatchQueue.main.async {
                self?.users = users
                self?.tableView.reloadData()
            }
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        fetchDataFirebaseController?.removeAllObservers()
        fetchDataFirebaseController = nil
    }
    
    //MARK: - Object lifecycle
    deinit {
        print("ContactsViewController was deleted!")
    }
}

//MARK: - UITableView DataSource
extension ContactsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactsCell", for: indexPath) as! ContactsCell
        let user = users?[indexPath.row]
        cell.user = user
        return cell
    }
}

//MARK: - UITableView Delegate
extension ContactsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedUser = users?[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
}

//MARK: - Prepare for segue
extension ContactsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "chatLogSegue" {
            let navController = segue.destination as! UINavigationController
            guard let chatLogVC = navController.viewControllers.first as? ChatLogViewController else {return}
            chatLogVC.selectedUser = selectedUser
        }
    }
}
