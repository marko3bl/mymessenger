//
//  LoginError.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation

enum LoginError: Error {
    case loginUserError(String)
    
    func getErrorMessage() -> String {
        switch self {
        case .loginUserError(let message):
            return message
        }
    }
}
