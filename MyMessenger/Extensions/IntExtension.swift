//
//  IntExtension.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation

extension Int {

    func convertToDate() -> String {
        let dateFormatter = DateFormatter()
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let currentCalendar = Calendar.current
        
        let messageDayDifference = Calendar.current.dateComponents([.day], from: Date(), to: date)
        
        guard let difference = messageDayDifference.day else {return ""}
        
        let numOfDays = abs(difference)
        
        if currentCalendar.isDateInToday(date) {
            dateFormatter.timeStyle = .short
            return dateFormatter.string(from: date)
        } else if currentCalendar.isDateInYesterday(date) {
            return "Yesterday"
        } else if numOfDays < 7 {
            dateFormatter.dateFormat = "EEEE"
            return dateFormatter.string(from: date)
        } else {
            dateFormatter.dateFormat = "dd/MM/yy"
            return dateFormatter.string(from: date)
        }
    }
    
    fileprivate func dateFallsInCurrentWeek(date: Date) -> Bool {
        
        let currentWeek = Calendar.current.component(.weekOfYear, from: Date())
        let datesWeek = Calendar.current.component(.weekOfYear, from: date)

        return currentWeek == datesWeek
    }
    
    fileprivate func isDifferenceInNumberOfDaysLessThanSeven(date: Date) -> Bool {
        let currentDay = Calendar.current.component(.day, from: Date())
        let dayFromDate = Calendar.current.component(.day, from: date)
        let difference = abs(currentDay-dayFromDate)
        print("Difference in number of days = \(difference)")
        return difference < 7
    }
}
