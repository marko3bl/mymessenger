//
//  UIImageViewExtension.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

fileprivate let imageCache = NSCache<AnyObject, AnyObject>()

func clearCach() {
    imageCache.removeAllObjects()
}

extension UIImageView {

    func loadImageUsingCacheWith(urlString: String) {

        image = nil

        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            image = cachedImage
            return
        }

        guard let url = URL(string: urlString) else {return}

        URLSession.shared.dataTask(with: url) { (data, response, error) in

            if error != nil {
                print(error!.localizedDescription)
                return
            }

            guard let serverData = data, let imageData = UIImage(data: serverData) else {return}

            DispatchQueue.main.async {
                imageCache.setObject(imageData, forKey: urlString as AnyObject)
                self.image = imageData
            }

        }.resume()
    }
}

class CustomImageView: UIImageView {
    
    weak var activityIndicatorDelegate: ActivityIndicatorDelegate?
    
    private var imageUrlString: String?
    
    func loadImageWith(urlString: String) {
        
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        activityIndicatorDelegate?.startAnimating()
        
        image = nil
        
        imageUrlString = urlString
        
        if let cachedImage = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            image = cachedImage
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            activityIndicatorDelegate?.stopAnimating()
            return
        }
        
        guard let url = URL(string: urlString) else {return}
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            guard let serverData = data, let imageData = UIImage(data: serverData) else {return}
            
            DispatchQueue.main.async {
                
                if self.imageUrlString == urlString {
                    self.image = imageData
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    self.activityIndicatorDelegate?.stopAnimating()
                }
                
                imageCache.setObject(imageData, forKey: urlString as AnyObject)

            }
            
            }.resume()
    }
}
