//
//  ImageMessageCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 12/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

protocol ZoomImageViewDelegate: class {
    func performZoomInFor(imageView: UIImageView)
}

class ImageMessageCell: BasicCell {

    weak var delegate: ZoomImageViewDelegate?
    
    var message: Message! {
        didSet {
            guard let url = message.imageUrl else {return}
            messageImageView.loadImageWith(urlString: url)
        }
    }
    
    var currentUserId: String! {
        didSet {
            bubbleView.backgroundColor = .clear
            super.alignmentBubbleView(userId: currentUserId, fromId: message.fromID)
            
        }
    }
    
    private lazy var messageImageView: CustomImageView = {
        let imageView = CustomImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 12
        imageView.layer.masksToBounds = true
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
        imageView.backgroundColor = UIColor.groupTableViewBackground
        imageView.activityIndicatorDelegate = self
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        bubbleView.addSubview(messageImageView)
        
        messageImageView.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        messageImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
        leadingConstraint = messageImageView.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 8)
        trailingConstraint = messageImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8)
        messageImageView.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        
        bubbleView.topAnchor.constraint(equalTo: messageImageView.topAnchor, constant: 0).isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: messageImageView.bottomAnchor, constant: 0).isActive = true
        bubbleView.leadingAnchor.constraint(equalTo: messageImageView.leadingAnchor, constant: 0).isActive = true
        bubbleView.trailingAnchor.constraint(equalTo: messageImageView.trailingAnchor, constant: 0).isActive = true
        
        setupLayoutForActivityIndicatorView(superView: bubbleView, height: 40, width: 40)
    }
    
    @objc func handleZoomTap(tapGestrure: UITapGestureRecognizer) {
        guard let imageView = tapGestrure.view as? UIImageView else {return}
        delegate?.performZoomInFor(imageView: imageView)
    }
}
