//
//  BasicCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 12/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

protocol ActivityIndicatorDelegate: class {
    func startAnimating()
    func stopAnimating()
}

class BasicCell: UITableViewCell {
    
    var bubbleView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.layer.cornerRadius = 12
        return view
    }()
    
    var profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.cornerRadius = 15
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    var activityIndicatorView: UIActivityIndicatorView = {
        let aiv = UIActivityIndicatorView(style: .whiteLarge)
        aiv.color = UIColor.darkGray
        aiv.translatesAutoresizingMaskIntoConstraints = false
        aiv.hidesWhenStopped = true
        return aiv
    }()
    
    var leadingConstraint: NSLayoutConstraint!
    var trailingConstraint: NSLayoutConstraint!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
    
    func setupViews() {
        
        self.backgroundColor = .clear
        
        addSubview(profileImageView)
        addSubview(bubbleView)
        
        profileImageView.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        profileImageView.topAnchor.constraint(equalTo: bubbleView.topAnchor, constant: 0).isActive = true
    }
    
    func alignmentBubbleView(userId: String, fromId: String) {
        if userId == fromId {
            leadingConstraint.isActive = false
            trailingConstraint.isActive = true
        } else {
            trailingConstraint.isActive = false
            leadingConstraint.isActive = true
        }
    }
    
    func setupLayoutForActivityIndicatorView(superView: UIView, height: CGFloat, width: CGFloat) {
        superView.addSubview(activityIndicatorView)
        activityIndicatorView.centerXAnchor.constraint(equalTo: superView.centerXAnchor).isActive = true
        activityIndicatorView.centerYAnchor.constraint(equalTo: superView.centerYAnchor).isActive = true
        activityIndicatorView.heightAnchor.constraint(equalToConstant: height).isActive = true
        activityIndicatorView.widthAnchor.constraint(equalToConstant: width).isActive = true
    }
    
}

extension BasicCell: ActivityIndicatorDelegate {
    
    func startAnimating() {
        activityIndicatorView.startAnimating()
    }
    
    func stopAnimating() {
        activityIndicatorView.stopAnimating()
    }
}
