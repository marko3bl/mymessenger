//
//  SendMessageFirebaseController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation
import Firebase


class SendMessageFirebaseController {
    
    private var databaseRef: DatabaseReference!
    weak var updateUIDelegate: UpdateUIDelegate?
    
    private var currentUserUUID: String? {
        return Auth.auth().currentUser?.uid
    }
    
    init() {
        databaseRef = Database.database().reference().child("messages")
    }
    
    private func sendMessageWith(properties: [String: Any]) {
        guard currentUserUUID != nil else {return}
        let timestamp = Int(Date().timeIntervalSince1970)
        let messageNode = databaseRef.childByAutoId()
        var values: [String: Any] = ["fromID": currentUserUUID!, "timestamp": timestamp, "isSeen": false]
        
        properties.forEach({values[$0] = $1})
        
        guard let toID = values["toID"] as? String else {return}
        
        messageNode.updateChildValues(values) { (error, ref) in
            
            if error != nil {
                print(error!.localizedDescription)
                return
            }
            
            let messageID = [messageNode.key: 1]
            
            let senderMessageRef = Database.database().reference().child("user-messages").child(self.currentUserUUID!).child(toID)
            
            senderMessageRef.updateChildValues(messageID, withCompletionBlock: { (error, ref) in
                if error != nil {
                    print(error!.localizedDescription)
                    return
                }
            })
            
            let recipientMessageRef = Database.database().reference().child("user-messages").child(toID).child(self.currentUserUUID!)
            
            recipientMessageRef.updateChildValues(messageID, withCompletionBlock: { (error, ref) in
                if error != nil {
                    print(error!.localizedDescription)
                    return
                }
            })
        }
    }
    
    func sendMessage(text: String, toID: String) {
        let properties: [String: Any] = ["text": text, "toID": toID]
        sendMessageWith(properties: properties)
    }
    
    func sendMessageWithImage(_ image: UIImage, toId: String) {
        let imageName = UUID().uuidString
        guard let uploadData = image.jpegData(compressionQuality: 0.8) else {return}
        
        let storageRef = createStorageRef(path: "message_images", fileName: imageName, fileExtension: ".jpg")
        
        storageRef.putData(uploadData, metadata: nil) { (metadata, error) in

            if error != nil {
                print("Faild to upload image: \(error!.localizedDescription)")
            }
            
            storageRef.downloadURL { (url, error) in
                if error != nil {
                    print("Faild to find download url: \(error!)")
                } else {
                    let imageUrl = url!.absoluteString
                    let properties: [String: Any] = ["toID": toId, "imageUrl": imageUrl, "imageHeight": image.size.height, "imageWidth": image.size.width]
                    self.sendMessageWith(properties: properties)
                }
            }
        }
    }
    
    func sendVideoMessage(_ videoUrl: URL, toId: String) {
        let videoName = UUID().uuidString
        let thumbnailImage = ThumbnailImage(fileUrl: videoUrl)
        let group = DispatchGroup()
        
        let storageRef = createStorageRef(path: "message_videos", fileName: videoName, fileExtension: ".mov")
        
        guard let thumbnail = thumbnailImage.createThumbnailImageOfVideoFromUrl() else {return}
        guard let uploadData = thumbnail.jpegData(compressionQuality: 0.8) else {return}
        
        let uploadTask = uploadVideo(to: storageRef, from: videoUrl)
        var videoUrlString = ""
        var imageUrlString = ""
        
        group.enter()
        
        self.getDataRemoteUrlString(data: uploadData, path: "message_images", fileExtension: ".jpg", group: group, completion: { (fileUrl) in
            
            guard let imageUrl = fileUrl else {return}
            imageUrlString = imageUrl
            group.leave()
        })
        
        group.enter()
        storageRef.downloadURL { (url, error) in
            if error != nil {
                print("Faild to find download url: \(error!)")
                group.leave()
            } else {
                videoUrlString = url!.absoluteString
                group.leave()
            }
        }
        
        group.notify(queue: DispatchQueue.global()) {
            let properties: [String: Any] = ["imageUrl": imageUrlString, "toID": toId, "videoUrl": videoUrlString, "imageWidth": thumbnail.size.width, "imageHeight": thumbnail.size.height]
            self.sendMessageWith(properties: properties)
        }
        
        //observe upload progress status
        
        uploadTask?.observe(.progress) { (snapshot) in
            let totalUnitCount = snapshot.progress!.totalUnitCount
            self.updateUIDelegate?.updateUI(value: snapshot.progress!.completedUnitCount, totalValue: totalUnitCount)
        }
        
        //observe upload success status
        
        uploadTask?.observe(.success) { (snapshot) in
            self.updateUIDelegate?.completedUpdate(true)
        }
        
    }
    
    private func uploadVideo(to storage: StorageReference, from url: URL) -> StorageUploadTask? {
        guard let videoData = try? Data(contentsOf: url) else { return nil }
        return storage.putData(videoData)
    }
    
    private func getDataRemoteUrlString(data: Data, path: String, fileExtension: String, group: DispatchGroup, completion: @escaping (_ urlString: String?) -> Void) {
        let name = UUID().uuidString
        let storageRef = createStorageRef(path: path, fileName: name, fileExtension: fileExtension)
        
        group.enter()
        storageRef.putData(data, metadata: nil) { (metadata, error) in
            
            if error != nil {
                print(error!.localizedDescription)
                completion(nil)
                group.leave()
                return
            }
            
    //        guard let url = metadata?.downloadURL()?.absoluteString else { return completion(nil) }
            
            storageRef.downloadURL { (url, error) in
                if error != nil {
                    print("Faild to find download url: \(error!)")
                    completion(nil)
                    group.leave()
                } else {
                    completion(url?.absoluteString)
                    group.leave()
                }
            }
        }
    }
    
    private func createStorageRef(path: String, fileName: String, fileExtension: String) -> StorageReference {
        let name = fileName + fileExtension
        return Storage.storage().reference().child(path).child(name)
    }
}
