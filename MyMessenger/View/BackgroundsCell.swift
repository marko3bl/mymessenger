//
//  BackgroundsCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/17/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class BackgroundsCell: UICollectionViewCell {
    
    @IBOutlet weak var backbroundImageView: UIImageView! {
        didSet {
            setup()
        }
    }
    
    @IBOutlet weak var checkedImageView: UIImageView!
    
    var backgroundName: String! {
        didSet {
            backbroundImageView.image = UIImage(named: backgroundName)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)

    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    //    fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setup() {
        backbroundImageView.layer.borderColor = UIColor.black.cgColor
        backbroundImageView.layer.borderWidth = 0.5
    }
    
}
