//
//  UIColorExtension.swift
//  MyMessenger
//
//  Created by Marko Tribl on 12/3/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var userMessageBubbleColor: UIColor {
        return UIColor(red: 74/255, green: 144/255, blue: 226/255, alpha: 1)
    }
    
    static var chatPartnerMessageBubbleColor: UIColor {
        return UIColor(red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
    }
    
    static var defaultBackgroundChatLogColor: UIColor {
        return UIColor(red: 239/255, green: 239/255, blue: 244/255, alpha: 1)
    }
    
    static var darkNavyColor: UIColor {
        return UIColor(red: 0/255, green: 27/255, blue: 72/255, alpha: 1)
    }
    
    static var navyColor: UIColor {
        return UIColor(red: 2/255, green: 69/255, blue: 122/255, alpha: 1)
    }
    
    static var blueColor: UIColor {
        return UIColor(red: 1/255, green: 138/255, blue: 190/255, alpha: 1)
    }
    
    static var softBlueColor: UIColor {
        return UIColor(red: 151/255, green: 202/255, blue: 219/255, alpha: 1)
    }
    
    static var softLightBlueColor: UIColor {
        return UIColor(red: 214/255, green: 232/255, blue: 238/255, alpha: 1)
    }
}
