//
//  ThumbnailImage.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/8/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit
import AVFoundation


class ThumbnailImage {
    
    private let fileUrl: URL
    private let asset: AVAsset
    private let imageGenerator: AVAssetImageGenerator
    
    init(fileUrl: URL) {
        self.fileUrl = fileUrl
        asset = AVAsset(url: fileUrl)
        imageGenerator = AVAssetImageGenerator(asset: asset)
        imageGenerator.appliesPreferredTrackTransform = true
    }
    
    func createThumbnailImageOfVideoFromUrl() -> UIImage? {
        
        let time = CMTimeMake(value: 1, timescale: 60)
        do {
            let image = try imageGenerator.copyCGImage(at: time, actualTime: nil)
            let thumbnailImage = UIImage(cgImage: image)
            return thumbnailImage
            
        } catch let error {
            print(error.localizedDescription)
            return nil
        }
    }
    
}
