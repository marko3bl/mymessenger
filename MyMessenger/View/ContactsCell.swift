//
//  ContactsCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/19/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class ContactsCell: UITableViewCell {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var profileImageView: UIImageView!
    @IBOutlet private weak var usernameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!
    
    //MARK: - Set up cell with data
    var user: User? {
        didSet {
            guard let userData = user else {return}
            profileImageView.loadImageUsingCacheWith(urlString: userData.profileImageURL)
            usernameLabel.text = userData.name
            emailLabel.text = userData.email
        }
    }
    
}
