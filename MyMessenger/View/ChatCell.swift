//
//  ChatCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class ChatCell: UITableViewCell {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var timeOfLastMessageLabel: UILabel!
    
    var message: Message! {
        didSet {
            
            updateLabels(for: message)
            
            if message.videoUrl != nil {
                lastMessageLabel.text = "Video message"
            } else if message.text != "" {
                lastMessageLabel.text = message.text
            } else if message.imageUrl != nil {
                lastMessageLabel.text = "Photo message"
            }
            
            timeOfLastMessageLabel.text = message.timestamp.convertToDate()
        }
    }
    
    private func updateLabels(for message: Message) {
        lastMessageLabel.font = message.isSeen ?? false ? UIFont.systemFont(ofSize: 15, weight: .regular) : UIFont.systemFont(ofSize: 15, weight: .bold)
        userNameLabel.font = message.isSeen ?? false ? UIFont.systemFont(ofSize: 17, weight: .semibold) : UIFont.systemFont(ofSize: 17, weight: .bold)
        timeOfLastMessageLabel.font = message.isSeen ?? false ? UIFont.systemFont(ofSize: 14, weight: .regular) : UIFont.systemFont(ofSize: 14, weight: .bold)
    }
    
    var user: User? {
        didSet {
            guard let user = user else {return}
            profileImageView.loadImageUsingCacheWith(urlString: user.profileImageURL)
            userNameLabel.text = user.name
        }
    }
    

}
