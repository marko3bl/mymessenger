//
//  BackgroundsViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/17/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class BackgroundsViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    private let userDefaults = UserDefaults.standard
    
    private var selectedBackgroundImageName: String? {
        return userDefaults.value(forKey: "backgroundImageKey") as? String
    }

    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        setupBackButton()
    }
    
    //MARK: - Helpers
    private func setupBackButton() {
        let backButton = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(backButtonTapped))
        navigationItem.leftBarButtonItem = backButton
    }

    @objc private func backButtonTapped() {
        print("cancel button tapped")
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Object lifecycle
    deinit {
        print("BackgroundsVC was deleted!")
    }

}

//MARK: - UICollectionView DataSource
extension BackgroundsViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "backgroundsCell", for: indexPath) as! BackgroundsCell
        let name: String = "background_\(indexPath.item + 1)"
        
        cell.checkedImageView.isHidden = selectedBackgroundImageName == name ? false : true
        cell.backgroundName = name
    
        return cell
    }
}

//MARK: - UICollectionView Delegate
extension BackgroundsViewController: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width / 2 , height: collectionView.frame.height / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "chooseBackgroundSegue", sender: self)
    }
}

//MARK: - BackgroundsViewController prepares for transition
extension BackgroundsViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "chooseBackgroundSegue" else {return}
        let vc = segue.destination as! ChooseBackgroundViewController
        guard let indexPath = collectionView.indexPathsForSelectedItems?.first else {return}
        let imageName = "background_\(indexPath.item + 1)"
        vc.imageName = imageName
    }
}
