//
//  MoreTableViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/12/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit


class MoreTableViewController: UITableViewController {
    
    //MARK: - IBOutlet
    @IBOutlet weak var backgroundImageView: UIImageView! {
        didSet {
            guard let imageName = backgroundImageName else {return}
            backgroundImageView.image = UIImage(named: imageName)
        }
    }
    
    //MARK: - Properties
    private var logoutFirebaseController: LogoutFirebaseController?

    private var backgroundImageName: String? {
        return UserDefaults.standard.value(forKey: "backgroundImageKey") as? String
    }
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "More"
        tableView.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        logoutFirebaseController = LogoutFirebaseController()
    }
    
    //MARK: - UITableView Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                print("Account settings")
            case 1:
                print("Change a background color")
            case 2:
                print("QR code scanner")
            case 3:
                print("About")
            case 4:
                showActionSheet()
            default:
                break
            }
        }
    }
    
    //MARK: - Object Lifecycle
    deinit {
        print("MoreTableViewController was deleted!")
    }
    
    //MARK: - IBAction UnwindSegue
    @IBAction func unwindToMoreVC(segue: UIStoryboardSegue) {
        guard let sourceVC = segue.source as? ChooseBackgroundViewController, let imageName = sourceVC.imageName else {return}
        backgroundImageView.image = UIImage(named: imageName)
    }
}

//MARK: - Show Action Sheet method
extension MoreTableViewController {
    
    private func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: "Are you sure you want to log out?", preferredStyle: .actionSheet)
        let logOutAction = UIAlertAction(title: "Log out", style: .destructive) { [weak self] (action) in
            self?.signOutButtonTapped()
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default) { [weak self] (action) in
            self?.navigationController?.popViewController(animated: true)
        }
        
        actionSheet.addAction(logOutAction)
        actionSheet.addAction(cancel)
        
        present(actionSheet, animated: true, completion: nil)
        
    }
}

//MARK: - Sign out method
extension MoreTableViewController {
    
    private func signOutButtonTapped() {
        logoutFirebaseController?.signOut(completion: { [weak self] (error) in
            if error != nil {
                self?.showAlert(with: error!)
                return
            }
            
            self?.dismiss(animated: true, completion: nil)
            clearCach()
        })
    }

}
