//
//  Message.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/25/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation

struct Message {
    let text: String
    let fromID: String
    let toID: String
    let timestamp: Int
    
    var imageUrl: String?
    var imageHeight: NSNumber?
    var imageWidth: NSNumber?
    
    var videoUrl: String?
    
    var isSeen: Bool? = false
    
    init(dictionary: [String: Any]) {
        text = dictionary["text"] as? String ?? ""
        fromID = dictionary["fromID"] as? String ?? ""
        toID = dictionary["toID"] as? String ?? ""
        timestamp = dictionary["timestamp"] as? Int ?? 0
        imageUrl = dictionary["imageUrl"] as? String
        imageHeight = dictionary["imageHeight"] as? NSNumber
        imageWidth = dictionary["imageWidth"] as? NSNumber
        videoUrl = dictionary["videoUrl"] as? String
        isSeen = dictionary["isSeen"] as? Bool
    }
    
    func chatPartnerID(userId: String?) -> String? {
        guard let id = userId else {return nil}
        return id == fromID ? toID : fromID
    }
}
