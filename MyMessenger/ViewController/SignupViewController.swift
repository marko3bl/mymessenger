//
//  SignupViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/12/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var profileImageView: UIImageView!
    
    //MARK: - Properties
    private var signupFirebaseController: SignupFirebaseController?
    private var userProfileImage: UIImage!
    
    //MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupProfileImageViewTapRegognizer()
        userProfileImage = UIImage(named: "UserIcon")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        signupFirebaseController = SignupFirebaseController()
        setupTextFiledDelegate(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        signupFirebaseController = nil
        setupTextFiledDelegate(false)
    }
    
    //MARK: - IBAction
    //MARK: Sign up button method
    @IBAction func signUpButtonTapped(_ sender: UIButton) {
        signUp()
    }
    
    //MARK: Sign in button method
    @IBAction func cancelSignup(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    //MARK: - Helper methods
    private func areAllFieldsFilled() -> Bool {
        guard usernameTextField.text != "", passwordTextField.text != "", emailTextField.text != "", userProfileImage != nil  else {
            return false
        }
        return true
    }
    
    private func setupTextFiledDelegate(_ isNeedDelegate: Bool) {
        usernameTextField.delegate = isNeedDelegate ? self : nil
        passwordTextField.delegate = isNeedDelegate ? self : nil
        emailTextField.delegate = isNeedDelegate ? self : nil
    }
    
    private func setupProfileImageViewTapRegognizer() {
        profileImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(profileImageTapped)))
    }
    
    @objc private func profileImageTapped() {
        let picker = UIImagePickerController()
        picker.delegate = self
        picker.allowsEditing = true
        present(picker, animated: true, completion: nil)
    }
    
    private func signUp() {
        if areAllFieldsFilled() {
            signupFirebaseController?.createAccount(username: usernameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, profileImage: userProfileImage, completion: { (error) in
                
                if error != nil {
                    self.showAlert(with: error!.getMessage())
                } else {
                    self.dismiss(animated: true, completion: nil)
                }
            })
        } else {
            showAlert(with: "Please fill all fields!")
        }
    }
    
    //MARK: - View method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK: - Object lifecycle
    deinit {
        print("SignupViewController was deleted!")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: - UITextField delegate
extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let next = textField.superview?.viewWithTag(textField.tag + 1) {
            next.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            signUp()
            return true
        }
        return false
    }
}

//MARK: - Implement UIImagePickerControllerDelegate
extension SignupViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let editingImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            profileImageView.image = editingImage
            userProfileImage = editingImage
        } else if let originalImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            profileImageView.image = originalImage
            userProfileImage = originalImage
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
