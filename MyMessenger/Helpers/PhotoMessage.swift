//
//  PhotoMessage.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/7/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

class PhotoMessage: NSObject {
    
    private var startingFrame: CGRect!
    private var startingImageView: UIImageView!
    private var startingKeyWindowFrame: CGRect!
    
    private var zoomingImageView: UIImageView! {
        didSet {
            zoomingImageView.translatesAutoresizingMaskIntoConstraints = false
            zoomingImageView.isUserInteractionEnabled = true
            zoomingImageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomOut)))
            zoomingImageView.contentMode = .scaleAspectFill
        }
    }
    
    //MARK: - UI properties
    private var blackBackgroundView: UIView! {
        didSet {
            blackBackgroundView.translatesAutoresizingMaskIntoConstraints = false
            blackBackgroundView.addSubview(closeButton)
            closeButton.topAnchor.constraint(equalTo: blackBackgroundView.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
            closeButton.leadingAnchor.constraint(equalTo: blackBackgroundView.safeAreaLayoutGuide.leadingAnchor, constant: 16).isActive = true
            closeButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
            closeButton.widthAnchor.constraint(equalToConstant: 40).isActive = true
        }
    }
    
    private lazy var closeButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "close"), for: .normal)
        button.tintColor = UIColor.groupTableViewBackground
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleZoomOutWithButton), for: .touchUpInside)
        return button
    }()
    
    //MARK: - ZoomImage Width and Height Constraints
    private var zoomImageViewHeightConstraint: NSLayoutConstraint! {
        didSet {
            zoomImageViewHeightConstraint.isActive = true
        }
    }
    
    private var zoomImageViewWidthConstraint: NSLayoutConstraint! {
        didSet {
            zoomImageViewWidthConstraint.isActive = true
        }
    }
    
    //MARK: - Methods for zooming out image
    @objc private func handleZoomOut(tapGesture: UITapGestureRecognizer) {
        
        guard let zoomOutImageview = tapGesture.view as? UIImageView else {return}
        handleZoomOutImage(imageView: zoomOutImageview)
    }
    
    @objc private func handleZoomOutWithButton() {
        handleZoomOutImage(imageView: zoomingImageView)
    }
    
    private func handleZoomOutImage(imageView: UIImageView) {
        imageView.layer.cornerRadius = 12
        imageView.layer.masksToBounds = true
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            imageView.frame = self.startingFrame
            self.blackBackgroundView.alpha = 0
            
        }) { (completed) in
            imageView.removeFromSuperview()
            self.blackBackgroundView.removeFromSuperview()
            self.startingImageView.isHidden = false
        }
    }
    
    //MARK: - API for handling zoom image frame that depend on phone orientation
    func setupImageHeightInLandscapeMode(frame: CGRect) {
        
        guard startingFrame != nil else {return}
        
        let startingHeight = startingFrame.height / startingFrame.width * startingKeyWindowFrame.width
        let ratio = startingFrame.height / startingFrame.width
        
        if UIDevice.current.orientation.isLandscape {
            
            let width = ratio * frame.width
            zoomImageViewHeightConstraint.constant = frame.width
            zoomImageViewWidthConstraint.constant = width
            
        } else {
            zoomImageViewHeightConstraint.constant = startingHeight
            zoomImageViewWidthConstraint.constant = startingKeyWindowFrame.width
        }
    }

}

//MARK: - Implement ZoomImageViewDelegate
extension PhotoMessage: ZoomImageViewDelegate {
    
    func performZoomInFor(imageView: UIImageView) {
        
        startingImageView = imageView
        startingImageView.isHidden = true
        
        startingFrame = imageView.superview?.convert(imageView.frame, to: nil)

        zoomingImageView = UIImageView()
        zoomingImageView.image = imageView.image
        
        guard let keyWindow = UIApplication.shared.keyWindow else {return}
  
        startingKeyWindowFrame = keyWindow.frame
        
        blackBackgroundView = UIView()
        blackBackgroundView.backgroundColor = .black
        blackBackgroundView.alpha = 0
        
        keyWindow.addSubview(blackBackgroundView)
        keyWindow.addSubview(zoomingImageView)
        
        zoomImageViewWidthConstraint = zoomingImageView.widthAnchor.constraint(equalToConstant: startingKeyWindowFrame.width)
        zoomImageViewHeightConstraint = zoomingImageView.heightAnchor.constraint(equalToConstant: startingFrame.height)
        zoomingImageView.centerYAnchor.constraint(equalTo: keyWindow.centerYAnchor).isActive = true
        zoomingImageView.centerXAnchor.constraint(equalTo: keyWindow.centerXAnchor).isActive = true
        
        blackBackgroundView.topAnchor.constraint(equalTo: keyWindow.topAnchor).isActive = true
        blackBackgroundView.trailingAnchor.constraint(equalTo: keyWindow.trailingAnchor).isActive = true
        blackBackgroundView.bottomAnchor.constraint(equalTo: keyWindow.bottomAnchor).isActive = true
        blackBackgroundView.leadingAnchor.constraint(equalTo: keyWindow.leadingAnchor).isActive = true
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 2, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackBackgroundView.alpha = 1
            keyWindow.bringSubviewToFront(self.closeButton)
            // calculate correct height
            let height = self.startingFrame.height / self.startingFrame.width * keyWindow.frame.width
            self.zoomImageViewHeightConstraint.constant = height
        })
    }
}
