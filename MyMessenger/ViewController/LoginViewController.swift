//
//  LoginViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/12/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    //MARK: - Properties
    private var loginFirebaseController: LoginFirebaseController?
    
    //MARK: - View lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loginFirebaseController = LoginFirebaseController()
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        loginFirebaseController = nil
        emailTextField.delegate = nil
        passwordTextField.delegate = nil
    }
    
    //MARK: - IBAction
    //MARK: Login button
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        signIn()
    }
    
    //MARK: Signup button
    @IBAction func createAccountButtonTapped(_ sender: UIButton) {
        performSegue(withIdentifier: "signupSegue", sender: self)
    }
    
    //MARK: - Helper methods
    private func signIn() {
        guard emailTextField.text != "", passwordTextField.text != "" else {
            showAlert(with: "Please fill all fields.")
            return
        }
        loginFirebaseController?.login(with: emailTextField.text!, password: passwordTextField.text!) { [weak self](success, error) in
            
            if success {
                self?.instantiateTabBarController()
                self?.clearTextFields()
            } else {
                self?.showAlert(with: error!.getErrorMessage())
            }
        }
    }
    
    private func clearTextFields() {
        emailTextField.text = nil
        passwordTextField.text = nil
    }
    
    private func instantiateTabBarController() {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let tabBarController = storyboard.instantiateViewController(withIdentifier: "tabBarcontroller")
        present(tabBarController, animated: true, completion: nil)
    }
    
    //MARK: - View method
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
    
    //MARK: - Object lifecycle
    deinit {
        print("LoginViewController was deleted!")
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

//MARK: UITextField delegate
extension LoginViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if let next = textField.superview?.viewWithTag(textField.tag + 1) {
            next.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            signIn()
            return true
        }
        
        return false
    }
}
