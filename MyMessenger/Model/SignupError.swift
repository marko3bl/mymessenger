//
//  SignupError.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation

enum SignupError {
    case createUserError(String)
    case userDataNotSet(String)
    case emptyTextFields(String)
    case uploadProfileImageError(String)
    
    func getMessage() -> String {
        switch  self {
        case .createUserError(let message):
            return message
        case .userDataNotSet(let message):
            return message
        case .emptyTextFields(let message):
            return message
        case .uploadProfileImageError(let message):
            return message
        }
    }
}
