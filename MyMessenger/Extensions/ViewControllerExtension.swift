//
//  ViewControllerExtension.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func showAlert(with message:String, title: String = "Alert") {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(action)
        present(alertController, animated: true, completion: nil)
    }
}
