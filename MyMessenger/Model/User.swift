//
//  User.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation

struct User {
    let name: String
    let email: String
    let profileImageURL: String
    var id: String?
    
    init(dictionary: [String: Any]) {
        name = dictionary["username"] as? String ?? ""
        email = dictionary["email"] as? String ?? ""
        profileImageURL = dictionary["profileImageURL"] as? String ?? ""
    }
}
