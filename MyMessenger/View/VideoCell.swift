//
//  VideoCell.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/8/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol VideoPlayerDelegate: class {
    func playVideo(url: URL)
}

class VideoCell: BasicCell {

    weak var delegate: ZoomImageViewDelegate?
    weak var videoPlayerDelegate: VideoPlayerDelegate?
    
    var message: Message! {
        didSet {
            guard let imageUrl = message.imageUrl else {return}
            thumbnailImageView.loadImageWith(urlString: imageUrl)
        }
    }
    
    var currentUserId: String! {
        didSet {
            bubbleView.backgroundColor = .clear
            super.alignmentBubbleView(userId: currentUserId, fromId: message.fromID)
            setupLayoutForActivityIndicatorView(superView: bubbleView, height: 40, width: 40)
        }
    }

    private lazy var thumbnailImageView: CustomImageView = {
        let civ = CustomImageView()
        civ.translatesAutoresizingMaskIntoConstraints = false
        civ.contentMode = .scaleAspectFill
        civ.layer.cornerRadius = 12
        civ.layer.masksToBounds = true
        civ.isUserInteractionEnabled = true
        civ.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(handleZoomTap)))
        civ.backgroundColor = UIColor.groupTableViewBackground
        civ.activityIndicatorDelegate = self
        return civ
    }()
    
    private lazy var playButton: UIButton = {
        let button = UIButton(type: .system)
        button.setImage(UIImage(named: "play"), for: .normal)
        button.tintColor = .white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handlePlay), for: .touchUpInside)
        return button
    }()
    
    override func setupViews() {
        super.setupViews()
        
        bubbleView.addSubview(thumbnailImageView)
        bubbleView.addSubview(playButton)
        
        thumbnailImageView.topAnchor.constraint(equalTo: topAnchor, constant: 4).isActive = true
        thumbnailImageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -4).isActive = true
        leadingConstraint = thumbnailImageView.leadingAnchor.constraint(equalTo: profileImageView.trailingAnchor, constant: 8)
        trailingConstraint = thumbnailImageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -8)
        thumbnailImageView.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        
        bubbleView.topAnchor.constraint(equalTo: thumbnailImageView.topAnchor, constant: 0).isActive = true
        bubbleView.bottomAnchor.constraint(equalTo: thumbnailImageView.bottomAnchor, constant: 0).isActive = true
        bubbleView.leadingAnchor.constraint(equalTo: thumbnailImageView.leadingAnchor, constant: 0).isActive = true
        bubbleView.trailingAnchor.constraint(equalTo: thumbnailImageView.trailingAnchor, constant: 0).isActive = true
        
        playButton.centerXAnchor.constraint(equalTo: bubbleView.centerXAnchor).isActive = true
        playButton.centerYAnchor.constraint(equalTo: bubbleView.centerYAnchor).isActive = true
        playButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        playButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    @objc func handleZoomTap(tapGesture: UITapGestureRecognizer) {
        guard let image = tapGesture.view as? UIImageView else {return}
        delegate?.performZoomInFor(imageView: image)
    }
    
    @objc func handlePlay() {
        guard let urlString = message.videoUrl, let url = URL(string: urlString) else {return}
        videoPlayerDelegate?.playVideo(url: url)
    }
}
