//
//  LogoutFirebaseController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import Foundation
import Firebase

class LogoutFirebaseController {
    

    func signOut(completion: @escaping(_ signOutError: String?) -> Void) {
        
        do {
            try Auth.auth().signOut()
            completion(nil)
        } catch let error {
            completion(error.localizedDescription)
        }
    }
    
    deinit {
        print("LogoutFirebaseController was deleted!")
    }
}
