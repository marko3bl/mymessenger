//
//  NavigationItemExtension.swift
//  MyMessenger
//
//  Created by Marko Tribl on 11/18/18.
//  Copyright © 2018 Marko Tribl. All rights reserved.
//

import UIKit

extension UINavigationItem {
    
    func setupTitleViewWith(name: String, profileImageURL: String) {
        let customTitleView = UIView()
        customTitleView.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        
        let containerView = UIView()
        containerView.translatesAutoresizingMaskIntoConstraints = false
        customTitleView.addSubview(containerView)
        
        containerView.centerYAnchor.constraint(equalTo: customTitleView.centerYAnchor).isActive = true
        containerView.centerXAnchor.constraint(equalTo: customTitleView.centerXAnchor).isActive = true
        
        //setup profile image view
        let profileImageView = UIImageView()
        profileImageView.translatesAutoresizingMaskIntoConstraints = false
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.layer.cornerRadius = 15
        profileImageView.layer.masksToBounds = true
        profileImageView.loadImageUsingCacheWith(urlString: profileImageURL)
        
        containerView.addSubview(profileImageView)
        
        profileImageView.leftAnchor.constraint(equalTo: containerView.leftAnchor).isActive = true
        profileImageView.centerYAnchor.constraint(equalTo: containerView.centerYAnchor).isActive = true
        profileImageView.widthAnchor.constraint(equalToConstant: 30).isActive = true
        profileImageView.heightAnchor.constraint(equalToConstant: 30).isActive = true
        
        //setup name label
        let nameLabel = UILabel()
        nameLabel.translatesAutoresizingMaskIntoConstraints = false
        nameLabel.text = name
        
        containerView.addSubview(nameLabel)
        
        nameLabel.leftAnchor.constraint(equalTo: profileImageView.rightAnchor, constant: 8).isActive = true
        nameLabel.centerYAnchor.constraint(equalTo: profileImageView.centerYAnchor).isActive = true
        nameLabel.rightAnchor.constraint(equalTo: containerView.rightAnchor).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 40).isActive = true
        
        titleView = customTitleView
    }
    
    func setupTitleViewWithProgressBar(view: UIView, alertText: String) {

        let nameLabel = UILabel()
        nameLabel.text = alertText
        nameLabel.font = UIFont.systemFont(ofSize: 15)
        nameLabel.textAlignment = .center
        
        let stackView = UIStackView()
        stackView.frame = CGRect(x: 0, y: 0, width: 200, height: 40)
        stackView.axis = .vertical
        stackView.alignment = .fill
        stackView.distribution = .equalSpacing
        stackView.spacing = 5
        
        stackView.addArrangedSubview(nameLabel)
        stackView.addArrangedSubview(view)
        
        titleView = stackView
    }
}
