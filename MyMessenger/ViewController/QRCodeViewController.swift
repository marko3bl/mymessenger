//
//  QRCodeViewController.swift
//  MyMessenger
//
//  Created by Marko Tribl on 1/28/19.
//  Copyright © 2019 Marko Tribl. All rights reserved.
//

import UIKit

protocol QRCodeProvider: class {
    func setupQrCodeFrameView(_ frame: CGRect)
    func found(code: String)
    func resetQRFrameView()
}

class QRCodeViewController: UIViewController {
    
    //MARK: - Properties
    private lazy var qrScanner = QRScanner(delegate: self)
    
    //MARK: - UI propertie
    private var qrCodeFrameView: UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.green.cgColor
        view.layer.borderWidth = 2
        return view
    }()

    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        qrScanner.initializeVideoPreviewLayer(view: view)
        qrScanner.startVideoCapture()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        qrScanner.stopVideoCapture()
    }
    
    //MARK: - IBAction
    @IBAction func cancelButtonTapped(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

//MARK: - Implement QRCodeProvider protocol
extension QRCodeViewController: QRCodeProvider {
    
    func resetQRFrameView() {
        DispatchQueue.main.async {
            self.qrCodeFrameView.frame = .zero
        }
    }
    
    func found(code: String) {
        showActionSheet(for: code)
    }
    
    func setupQrCodeFrameView(_ frame: CGRect) {
        
        qrCodeFrameView.frame = frame
        
        view.addSubview(qrCodeFrameView)
        view.bringSubviewToFront(qrCodeFrameView)
    }
}

//MARK: - Show Action Sheet
extension QRCodeViewController {
    
    func showActionSheet(for message: String) {
        
        let alertVC = UIAlertController(title: "QR code", message: message, preferredStyle: .actionSheet)
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { _ in
            self.qrScanner.startVideoCapture()
        }
        
        let copy = UIAlertAction(title: "Copy", style: .default) { _ in
            UIPasteboard.general.string = message
            self.qrScanner.startVideoCapture()
        }
        
        alertVC.addAction(cancel)
        alertVC.addAction(copy)
        
        guard let url = URL(string: message) else {
            present(alertVC, animated: true)
            return
        }
        
        let openURL = UIAlertAction(title: "Open", style: .default) { _ in
            self.open(url)
            self.qrScanner.startVideoCapture()
        }
        
        alertVC.addAction(openURL)
        present(alertVC, animated: true, completion: nil)
    }
    
    //MARK: - Helper method - Open URL
    func open(_ url: URL) {
        UIApplication.shared.open(url, options: [:])
    }
}
